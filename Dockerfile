FROM adoptopenjdk/openjdk11:alpine-jre
WORKDIR /opt/app
COPY target/*.jar .

ENV SERVER_PORT ${SERVER_PORT:-${PORT:-8080}}

EXPOSE ${SERVER_PORT}

CMD java ${JAVA_OPTS} -Djava.security.egd=file:/dev/./urandom -jar app.jar
