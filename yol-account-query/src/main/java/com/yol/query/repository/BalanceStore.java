package com.yol.query.repository;

import com.yol.query.model.Balance;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.util.UUID;

@Repository
public interface BalanceStore extends ReactiveMongoRepository<Balance, UUID> {
    Flux<Balance> findByAccountIdOrderByLastTransactionDateDesc(UUID accountId);
}
