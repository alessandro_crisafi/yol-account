package com.yol.query.event;

import lombok.Builder;
import lombok.Getter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.time.LocalDateTime;
import java.util.UUID;

/*
 * CREATE TABLE event (id uuid, entity_id uuid, entity_type text, source uuid, created_at timestamp, payload text, PRIMARY KEY(id, entity_id, created_at));
 */
@Table
@Getter
@Builder
public class Event {

    @PrimaryKey
    private final UUID id;

    @Column("entity_id")
    private UUID entityId;

    @Column("entity_type")
    private String entityType;

    private UUID source;

    @CreatedDate
    @Column("created_at")
    private LocalDateTime createdAt;

    @Column("payload")
    private String jsonPayload;
}