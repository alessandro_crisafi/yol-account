package com.yol.query.event;

import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.time.LocalDateTime;
import java.util.UUID;

@Repository
public interface EventStore extends ReactiveCassandraRepository<Event, UUID> {

    Flux<Event> findAllByEntityIdAndAndCreatedAtAfter(UUID uuid, LocalDateTime after);

    Flux<Event> findAllByEntityId(UUID accountId);
}