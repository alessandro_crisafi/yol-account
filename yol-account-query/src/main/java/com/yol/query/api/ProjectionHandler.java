package com.yol.query.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yol.query.event.Event;
import com.yol.query.event.EventStore;
import com.yol.query.model.Balance;
import com.yol.query.model.ValidTransaction;
import com.yol.query.repository.BalanceStore;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ProjectionHandler {

    private final BalanceStore balanceStore;

    private final EventStore eventStore;

    private final ObjectMapper objectMapper;

    public Mono<ServerResponse> getAccountBalance(ServerRequest serverRequest) {
        var accountId = UUID.fromString(serverRequest.pathVariable("account_id"));
        var snapShotControl = serverRequest.headers().header("Snapshot-Control");
        var forceUpdate = snapShotControl.stream().filter(header -> header.equals("force-update")).findFirst();

        if (forceUpdate.isPresent()) {
            return findLastOrCreateNewBalance(accountId)
                    .map(balance -> updateBalanceWithLatestTransactions(accountId, balance))
                    .flatMap(balance -> ServerResponse.ok().body(balance, Balance.class))
                    .switchIfEmpty(ServerResponse.notFound().build());
        }

        return balanceStore.findByAccountIdOrderByLastTransactionDateDesc(accountId)
                .next()
                .flatMap(balance -> ServerResponse.ok().bodyValue(balance))
                .switchIfEmpty(ServerResponse.notFound().build());
    }

    public Mono<ServerResponse> updateBalance(ServerRequest serverRequest) {
        var accountId = UUID.fromString(serverRequest.pathVariable("account_id"));
        return findLastOrCreateNewBalance(accountId)
                .map(balance -> updateBalanceWithLatestTransactions(accountId, balance))
                .flatMap(balance -> ServerResponse.ok().body(balance, Balance.class));
    }

    private Mono<Balance> findLastOrCreateNewBalance(UUID accountId) {
        return balanceStore.findByAccountIdOrderByLastTransactionDateDesc(accountId)
                .next()
                .switchIfEmpty(Mono.just(Balance.builder()
                        .id(UUID.randomUUID())
                        .accountId(accountId)
                        .amount(0D)
                        .build()));
    }

//** TODO NEED TO CHECK ENTITY TYPE AND ADD EVENT SEQUENCE NUMBER

    private Mono<Balance> updateBalanceWithLatestTransactions(UUID accountId, Balance previousSnapshot) {

        var builder = Balance.builder().accountId(previousSnapshot.getAccountId());

        return Optional.ofNullable(previousSnapshot.getLastTransactionDate())
                .map(lastUpdate -> eventStore.findAllByEntityIdAndAndCreatedAtAfter(accountId, previousSnapshot.getLastTransactionDate()))
                .orElse(eventStore.findAllByEntityId(accountId))
                .map(this::readPayload)
                .map(validTransaction -> {
                    builder.lastTransactionDate(validTransaction.getCreatedAt());
                    return validTransaction.getAmount();
                })
                .reduce(previousSnapshot.getAmount(), Double::sum)
                .doOnNext(builder::amount)
                .flatMap(updatedBalance -> balanceStore.save(builder.build()));
    }

    private ValidTransaction readPayload(Event event) {
        try {
            return objectMapper.readValue(event.getJsonPayload(), ValidTransaction.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }

}
