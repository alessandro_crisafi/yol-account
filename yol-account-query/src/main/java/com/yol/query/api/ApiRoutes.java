package com.yol.query.api;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
@RequiredArgsConstructor
public class ApiRoutes {

    private final ProjectionHandler handler;

    @Bean
    public RouterFunction<ServerResponse> functionalRoutes() {
        return route(GET("balance/{account_id}"), handler::getAccountBalance)
                .and(route(GET("accounts/{account_id}/_updateBalance"), handler::updateBalance));
    }
}
