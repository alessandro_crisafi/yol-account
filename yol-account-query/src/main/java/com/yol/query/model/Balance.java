package com.yol.query.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDateTime;
import java.util.UUID;

@Document
@Builder
@Getter
public class Balance implements Persistable<UUID> {

    @Id
    @Builder.Default
    private UUID id = UUID.randomUUID();

    private UUID accountId;

    @Field
    private double amount;

    @Field("last_transaction_date")
    @JsonProperty("last_transaction_date")
    private LocalDateTime lastTransactionDate;

    @Field("last_updated")
    @JsonProperty("last_updated")
    @Builder.Default
    private LocalDateTime lastUpdated = LocalDateTime.now();

    @JsonIgnore
    @Override
    public UUID getId() {
        return id;
    }

    @JsonIgnore
    @Override
    public boolean isNew() {
        return false;
    }
}
