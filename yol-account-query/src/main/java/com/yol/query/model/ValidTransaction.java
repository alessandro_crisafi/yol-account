package com.yol.query.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class ValidTransaction {

    @JsonProperty("transaction_id")
    private UUID transactionId;
    private Double amount;
    private UUID payer;
    private UUID payee;
    @JsonProperty("created_at")
    private LocalDateTime createdAt;
}