package com.yol.framework.event;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ResolvableType;
import org.springframework.core.ResolvableTypeProvider;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Transient;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import java.time.LocalDateTime;
import java.util.UUID;

/*
 * CREATE TABLE event (id uuid, entity_id uuid, entity_type text, source uuid, created_at timestamp, received_at timestamp, payload text, PRIMARY KEY(entity_id, created_at));
 */
@Table
@Getter
@Builder
public class Event<T> implements ResolvableTypeProvider {

    @PrimaryKey
    @Builder.Default
    private final UUID id = UUID.randomUUID();

    /**
     * idea: use class AccountId and make UUID a Generic T
     */
    @Column("entity_id")
    private UUID entityId;

    @Column("entity_type")
    private String entityType;

    @Column
    private UUID source;

    @Column("created_at")
    @CreatedDate
    private LocalDateTime createdAt;

    @Column("received_at")
    @CreatedDate
    private LocalDateTime received_at;

    @Getter(AccessLevel.NONE)
    @Column("payload")
    private String jsonPayload;

    @Transient
    private T payload;

    @Override
    public ResolvableType getResolvableType() {
        return ResolvableType.forClassWithGenerics(
                getClass(),
                ResolvableType.forInstance(this.payload)
        );
    }

    public static class EventBuilder<T> {

        @Autowired
        private final ObjectMapper objectMapper = Jackson2ObjectMapperBuilder.json().build();

        private String jsonPayload;
        private T payload;

        @SneakyThrows
        public EventBuilder payload(T payload) {
            this.payload = payload;
            this.jsonPayload = objectMapper.writeValueAsString(payload);
            return this;
        }


    }

}