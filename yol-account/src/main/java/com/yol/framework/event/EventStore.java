package com.yol.framework.event;

import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface EventStore extends ReactiveCassandraRepository<Event, UUID> {

}
