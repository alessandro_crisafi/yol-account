package com.yol.command.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
public class ApiRoutes {

    @Autowired
    private RestHandler handler;

    @Bean
    public RouterFunction<ServerResponse> functionalRoutes() {
        return route(POST("/command/submitTransaction"), req -> handler.submitTransaction(req));
    }
}
