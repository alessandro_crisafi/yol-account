package com.yol.command.api;

import com.yol.command.transaction.TransactionManager;
import com.yol.command.transaction.command.SubmitTransaction;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;


@Service
@RequiredArgsConstructor
public class RestHandler {

    public final TransactionManager transactionManager;

    public Mono<ServerResponse> submitTransaction(ServerRequest serverRequest) {
        return serverRequest.bodyToMono(SubmitTransaction.class)
                .flatMap(transactionManager::submitTransaction)
                .flatMap(event -> ServerResponse.ok().bodyValue(event.getPayload()));
    }

}
