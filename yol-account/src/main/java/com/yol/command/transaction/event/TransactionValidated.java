package com.yol.command.transaction.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.UUID;


@Builder
@Getter
public class TransactionValidated {

    @JsonProperty("transaction_id")
    private UUID transactionId;
    private Double amount;
    private UUID payer;
    private UUID payee;

    @JsonProperty("created_at")
    private LocalDateTime createdAt;
}