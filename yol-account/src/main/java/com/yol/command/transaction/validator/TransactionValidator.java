package com.yol.command.transaction.validator;

import com.yol.command.transaction.command.SubmitTransaction;
import org.springframework.stereotype.Component;

@Component
public class TransactionValidator {
    public boolean validate(SubmitTransaction submitTransaction) {
        return submitTransaction.getAmount() > 0D;
    }
}