package com.yol.command.transaction.saga;

import com.yol.command.transaction.event.TransactionValidated;
import org.springframework.context.event.EventListener;

public class QualifyTransactionSaga extends Saga {


    @EventListener
    private void handleTransactionValidated(TransactionValidated transactionValidated) {
        this.start();
    }

}
