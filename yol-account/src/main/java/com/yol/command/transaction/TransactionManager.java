package com.yol.command.transaction;

import com.yol.command.transaction.command.SubmitTransaction;
import com.yol.command.transaction.error.ApplicationError;
import com.yol.command.transaction.error.InvalidTransactionException;
import com.yol.command.transaction.event.TransactionValidated;
import com.yol.command.transaction.validator.TransactionValidator;
import com.yol.framework.event.Event;
import com.yol.framework.event.EventStore;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;

import java.time.Duration;
import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class TransactionManager {

    private final EventStore eventStore;
    private final TransactionValidator transactionValidator;
    private final ApplicationEventPublisher applicationEventPublisher;

    @Value("${transaction.event.subscriber}")
    private String projectorBaseUrl;

    @EventListener
    public void handle(Event<TransactionValidated> transactionValidatedEvent) {
        eventStore.save(transactionValidatedEvent).doOnNext(tx ->
                WebClient.builder().baseUrl(projectorBaseUrl).build()
                        .get().uri("/accounts/" + transactionValidatedEvent.getEntityId() + "/_updateBalance")
                        .exchange()
                        .retryWhen(Retry.backoff(3, Duration.ofSeconds(15))
                                .filter(this::is5xxServerError))
                        .log()
                        .subscribe())
                .subscribe();
    }

    public Mono<Event<TransactionValidated>> submitTransaction(SubmitTransaction submitTransaction) {
        return Mono.just(submitTransaction)
                .<Event<TransactionValidated>>handle((command, sink) -> {
                    if (transactionValidator.validate(submitTransaction)) {

                        sink.next(
                                Event.builder()
                                        .source(submitTransaction.getTransactionId())
                                        .entityType(TransactionValidated.class.getCanonicalName())
                                        .entityId(submitTransaction.getPayee())
                                        .createdAt(submitTransaction.getCreatedAt())
                                        .received_at(LocalDateTime.now())
                                        .payload(
                                                TransactionValidated.builder()
                                                        .transactionId(submitTransaction.getTransactionId())
                                                        .amount(submitTransaction.getAmount())
                                                        .payee(submitTransaction.getPayee())
                                                        .payer(submitTransaction.getPayer())
                                                        .createdAt(submitTransaction.getCreatedAt())
                                                        .build())
                                        .build());
                    } else {
                        sink.error(new InvalidTransactionException(command));
                    }
                })
                .doOnError(error -> applicationEventPublisher.publishEvent(new ApplicationError(error)))
                .doOnNext(applicationEventPublisher::publishEvent);
    }

    private boolean is5xxServerError(Throwable throwable) {
        return throwable instanceof WebClientResponseException &&
                ((WebClientResponseException) throwable).getStatusCode().is5xxServerError() ||
                throwable instanceof java.net.ConnectException;
    }

}
