package com.yol.command.transaction.error;

public class InvalidTransactionException extends Exception {

    private Object transaction;

    public InvalidTransactionException(Object tx) {
        this.transaction = tx;
    }
}
