package com.yol.command.transaction.error;

import org.springframework.context.ApplicationEvent;

/**
 * @author acrisafi on 22/10/2020
 */
public class ApplicationError extends ApplicationEvent {
    public ApplicationError(Throwable error) {
        super(error);
    }
}
