package com.yol.command.balance;

import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

@Getter
@Builder
public class UpdateBalance {
    private UUID accountId;
    private Double amount;
}
